/**
 * @biref To demonstrate how to use zutil.c and crc.c functions
 *
 * Copyright 2018-2020 Yiqing Huang
 *
 * This software may be freely redistributed under the terms of MIT License
 */

#include <stdio.h>    /* for printf(), perror()...   */
#include <stdlib.h>   /* for malloc()                */
#include <errno.h>    /* for errno                   */
#include "crc.h"      /* for crc()                   */
#include "zutil.h"    /* for mem_def() and mem_inf() */
#include "lab_png.h"  /* simple PNG data structures  */
#include <stdbool.h>  /* for bool type */
#include <arpa/inet.h>

/******************************************************************************
 * DEFINED MACROS 
 *****************************************************************************/
#define BUF_LEN  (256*16)
#define BUF_LEN2 (256*32)
#define HEADER_BYTES 8
#define IHDR_BYTES 25
#define IHDR_DATA_BYTES 13
#define LEN_BYTES 4
#define TYPE_BYTES 4
#define CRC_BYTES 4

/******************************************************************************
 * GLOBALS 
 *****************************************************************************/
U8 gp_buf_def[BUF_LEN2]; /* output buffer for mem_def() */
U8 gp_buf_inf[BUF_LEN2]; /* output buffer for mem_inf() */

/******************************************************************************
 * FUNCTION PROTOTYPES 
 *****************************************************************************/

void init_data(U8 *buf, int len);

/******************************************************************************
 * FUNCTIONS 
 *****************************************************************************/

/*
Convenience function for getting string representing type of chunk.
Useful for debugging. The callee is responsible for deallocating the memory located
at the returned address.
*/
char* get_type(U8 *type_chunk) {
    char *sp = malloc( TYPE_BYTES + 1 );
    size_t i;
    for ( i = 0; i < 4; ++i ) {
        sp[i] = (char)type_chunk[i];
    }
    sp[ TYPE_BYTES ] = '\0';
    return sp;
}

/*
Validates the crc section of a chunk.
Note that type_chunk must point to the length section of the chunk.
*/
bool validate_crc(U8 *type_chunk, uint32_t data_length) {
    char *type = get_type(type_chunk);
    printf("Validating %s chunk\n", type);
    free(type);
    printf("Data length %u\n", data_length);
    uint32_t crc_exp = crc(type_chunk, TYPE_BYTES + data_length);
    uint32_t crc_act = ntohl(*(uint32_t*)(type_chunk + TYPE_BYTES + data_length));
    if (crc_exp == crc_act) {
        printf("Expected crc %u matches the actual crc %u\n", crc_exp, crc_act);
        return true;
    } else {
        printf("CRC validation failed. Expected %u but got 0x%u\n", crc_exp, crc_act);
        return false;
    }
}

/* Returns the length section of this chunk converted to unsigned int */
uint32_t get_data_length(U8 *chunk) {
    return ntohl(*(uint32_t*)chunk);
}

bool is_iend(U8 *type_chunk) {
    size_t i;
    U8 iend[] = {0x49,  0x45,  0x4E,  0x44};
    for (i = 0; i < 4; ++i) {
        if (type_chunk[i] != iend[i]) {
            return false;
        }
    }
    return true;
}

/**
 * @brief initialize memory with 256 chars 0 - 255 cyclically 
 */
void init_data(U8 *buf, int len)
{
    int i;
    for ( i = 0; i < len; i++) {
        buf[i] = i%256;
    }
}


int main (int argc, char **argv){
    FILE *fp;
    U8 *p_buffer = NULL;

    U8 correctsignature[] = {0x89,  0x50,  0x4e,  0x47,  0xd,  0xa,  0x1a,  0xa};
    char* filename = argv[1];

    fp = fopen(filename, "r");
    if(fp == NULL){
        printf("Failed to open file %s \n", filename);
        return -1;
    }
    printf("Successfully opened file %s \n", filename);

    p_buffer = malloc(HEADER_BYTES + IHDR_BYTES);
    if (p_buffer == NULL) {
        perror("malloc");
	    return errno;
    }

    /* Storing entirety of header and IHDR chunk into p_buffer*/
    size_t bytesread = fread(p_buffer, 1, HEADER_BYTES + IHDR_BYTES, fp);

    /* Validate the IHDR chunk */
    if (!validate_crc(p_buffer + HEADER_BYTES + LEN_BYTES, IHDR_DATA_BYTES)) {
        free( p_buffer );
        return -1;
    }

    for(int i = 0 ; i < 8; i++){
        if(p_buffer[i] != correctsignature[i]){
            printf("%s: Not a PNG file\n", filename);
            free(p_buffer);
            return -1;
        }
    }

    printf("%s: 0x", filename);
    for(int i = 16; i < 20; i++){
        printf("%x", p_buffer[i]);
    }
    printf(" x 0x");
    for(int i = 20; i < 24; i++){
        printf("%x", p_buffer[i]);
    }
    printf("\n");

    /* Validating rest of the chunks until it reaches IEND */
    while(true) {
        /* Determine size of chunk and allocate
        memory to stream chunk into */
        fread(p_buffer, 1, LEN_BYTES, fp);
        uint32_t data_length = get_data_length(p_buffer);
        size_t chunk_size = TYPE_BYTES + data_length + CRC_BYTES;
        U8 *p_dbuffer = malloc( chunk_size );

        fread(p_dbuffer, 1, chunk_size, fp);
        if (!validate_crc(p_dbuffer, data_length)) {
            /* Do we need to free memory before returning from main? */
            free(p_dbuffer);
            free(p_buffer);
            return -1;
        }

        if (is_iend(p_dbuffer)) {
            free(p_dbuffer);
            break;
        }
        
        free(p_dbuffer);
    }

    free(p_buffer);
    fclose(fp);

    return 0;
}

// int main (int argc, char **argv)
// {
//     U8 *p_buffer = NULL;  /* a buffer that contains some data to play with */
//     U32 crc_val = 0;      /* CRC value                                     */
//     int ret = 0;          /* return value for various routines             */
//     U64 len_def = 0;      /* compressed data length                        */
//     U64 len_inf = 0;      /* uncompressed data length                      */
    
//     /* Step 1: Initialize some data in a buffer */
//     /* Step 1.1: Allocate a dynamic buffer */
//     p_buffer = malloc(BUF_LEN);
//     if (p_buffer == NULL) {
//         perror("malloc");
// 	return errno;
//     }

//     /* Step 1.2: Fill the buffer with some data */
//     init_data(p_buffer, BUF_LEN);

//     /* Step 2: Demo how to use zlib utility */
//     ret = mem_def(gp_buf_def, &len_def, p_buffer, BUF_LEN, Z_DEFAULT_COMPRESSION);
//     if (ret == 0) { /* success */
//         printf("original len = %d, len_def = %lu\n", BUF_LEN, len_def);
//     } else { /* failure */
//         fprintf(stderr,"mem_def failed. ret = %d.\n", ret);
//         return ret;
//     }
    
//     ret = mem_inf(gp_buf_inf, &len_inf, gp_buf_def, len_def);
//     if (ret == 0) { /* success */
//         printf("original len = %d, len_def = %lu, len_inf = %lu\n", \
//                BUF_LEN, len_def, len_inf);
//     } else { /* failure */
//         fprintf(stderr,"mem_def failed. ret = %d.\n", ret);
//     }

//     /* Step 3: Demo how to use the crc utility */
//     crc_val = crc(gp_buf_def, len_def); // down cast the return val to U32
//     printf("crc_val = %u\n", crc_val);
    
//     /* Clean up */
//     free(p_buffer); /* free dynamically allocated memory */

//     return 0;
// }
