/**
 * @biref To demonstrate how to use zutil.c and crc.c functions
 *
 * Copyright 2018-2020 Yiqing Huang
 *
 * This software may be freely redistributed under the terms of MIT License
 */

#include <stdio.h>    /* for printf(), perror()...   */
#include <stdlib.h>   /* for malloc()                */
#include <errno.h>    /* for errno                   */
#include "crc.h"      /* for crc()                   */
#include "zutil.h"    /* for mem_def() and mem_inf() */
#include "lab_png.h"  /* simple PNG data structures  */
#include <stdbool.h>  /* for bool type */
#include <arpa/inet.h>

/******************************************************************************
 * DEFINED MACROS 
 *****************************************************************************/
#define BUF_LEN  (4000000)
#define BUF_LEN2 (4000000)
#define HEADER_BYTES 8
#define IHDR_BYTES 25
#define IHDR_DATA_BYTES 13
#define LEN_BYTES 4
#define TYPE_BYTES 4
#define CRC_BYTES 4
#define IEND_BYTES 4

/******************************************************************************
 * GLOBALS 
 *****************************************************************************/
U8 gp_buf_def[BUF_LEN]; /* output buffer for mem_def() */
U8 gp_buf_inf[BUF_LEN2]; /* output buffer for mem_inf() */
U8 correctsignature[] = {0x89,  0x50,  0x4e,  0x47,  0xd,  0xa,  0x1a,  0xa}; // correct PNG File Header
U8 correctIHDR[] = { 0x0 , 0x0 , 0x0 , 0xd , 0x49 , 0x48 , 0x44 , 0x52 , 0x0 , 0x0 , 
                        0x1 , 0xc2 , 0x0 , 0x0 , 0x0 , 0xe5 , 0x8 , 0x6 , 0x0 , 0x0 , 
                        0x0 }; // correct IHDR
U8 correctIDAT[] = { 0x49 ,  0x44 ,  0x41 ,  0x54 };
U8 correctIEND[] = {0x0, 0x0, 0x0, 0x0, 0x49 , 0x45 , 0x4e , 0x44 , 0xae , 0x42 , 0x60 , 0x82}; // correct IEND

// 0x89 , 0x50 , 0x4e , 0x47 , 0xd , 0xa , 0x1a , 0xa , 0x0 , 0x0 , 0x0 , 0xd , 0x49 , 0x48 , 0x44 , 0x52 , 0x0 , 0x0 , 0x1 , 0xc2 , 0x0 , 0x0 , 0x0 , 0xe5 , 0x8 , 0x6 , 0x0 , 0x0 , 0x0 , 0xc0 , 0xc6 , 0xf7 , 0x7f

/******************************************************************************
 * FUNCTION PROTOTYPES 
 *****************************************************************************/

void init_data(U8 *buf, int len);

/******************************************************************************
 * FUNCTIONS 
 *****************************************************************************/

/*
Convenience function for getting string representing type of chunk.
Useful for debugging. The callee is responsible for deallocating the memory located
at the returned address.
*/
char* get_type(U8 *type_chunk) {
    char *sp = malloc( TYPE_BYTES + 1 );
    size_t i;
    for ( i = 0; i < 4; ++i ) {
        sp[i] = (char)type_chunk[i];
    }
    sp[ TYPE_BYTES ] = '\0';
    return sp;
}

/*
Validates the crc section of a chunk.
Note that type_chunk must point to the length section of the chunk.
*/
bool validate_crc(U8 *type_chunk, uint32_t data_length) {
    char *type = get_type(type_chunk);
    printf("Validating %s chunk\n", type);
    free(type);
    printf("Data length %u\n", data_length);
    uint32_t crc_exp = crc(type_chunk, TYPE_BYTES + data_length);
    uint32_t crc_act = ntohl(*(uint32_t*)(type_chunk + TYPE_BYTES + data_length));
    if (crc_exp == crc_act) {
        printf("Expected crc %u matches the actual crc %u\n", crc_exp, crc_act);
        return true;
    } else {
        printf("CRC validation failed. Expected %u but got 0x%u\n", crc_exp, crc_act);
        return false;
    }
}

/*
Validates the IHDR section of a png file
Note that p_buffer must point to the first section of the IHDR.
*/
bool validate_ihdr(U8 *p_buffer) {
    if (!validate_crc(p_buffer + HEADER_BYTES + LEN_BYTES, IHDR_DATA_BYTES)) {
        return false;
    }

    for(int i = 0 ; i < 8; i++){
        if(p_buffer[i] != correctsignature[i]){
            return false;
        }
    }

    return true;
}

/* Returns the length section of this chunk converted to unsigned int */
uint32_t get_data_length(U8 *chunk) {
    return ntohl(*(uint32_t*)chunk);
}

// returns the length of the png this header buffer pertains to
uint32_t get_png_length(U8 *p_hbuffer){
    U8 *length[4] = {0};

    memcpy(length, &p_hbuffer[20], 4);

    return ntohl(*(uint32_t*)length);
}

// returns the width of the png this header buffer pertains to
uint32_t get_png_width(U8 *p_hbuffer){
    U8 *length[4] = {0};

    memcpy(length, &p_hbuffer[16], 4);

    return ntohl(*(uint32_t*)length);
}

bool is_iend(U8 *type_chunk) {
    size_t i;
    U8 iend[] = {0x49,  0x45,  0x4E,  0x44};
    for (i = 0; i < 4; ++i) {
        if (type_chunk[i] != iend[i]) {
            return false;
        }
    }
    return true;
}

/**
 * @brief initialize memory with 256 chars 0 - 255 cyclically 
 */
void init_data(U8 *buf, int len)
{
    int i;
    for ( i = 0; i < len; i++) {
        buf[i] = i%256;
    }
}

U8* get_idat(FILE *fp_png, uint32_t data_length){
    U8 *p_ibuffer = NULL; // pointer used to iterate through IDAT section of PNG
    U8 *p_dbuffer = NULL; // pointer to data section of chunk


    /* Determine size of chunk and allocate
    memory to stream chunk into */
    size_t chunk_size = TYPE_BYTES + data_length + CRC_BYTES;
    p_ibuffer = malloc( chunk_size );

    fread(p_ibuffer, 1, chunk_size, fp_png);
    if (!validate_crc(p_ibuffer, data_length)) {
        free(p_ibuffer);
        p_ibuffer = NULL;
        exit(-1);
    }
        
    p_dbuffer = malloc(data_length);
    memcpy(p_dbuffer, &p_ibuffer[4], data_length);

    free(p_ibuffer);
    p_ibuffer = NULL;

    return p_dbuffer;

}


int main (int argc, char **argv){
    FILE *fp_png; // file pointer to the current png file we are operating on
    FILE *fp_all; // file pointer to the eventual concatenated png all.png
    U8 *p_hbuffer = NULL; // pointer to File Header and IHDR section of PNG
    U8 *p_lbuffer = NULL; // pointer to length section of chunk
    U8 *p_allbuffer = NULL; // pointer to IDAT of all.png file
    int ret = 0; // return value for various routines
    int bytesIngested = 0;
    uint32_t pngDataLength = 0;
    uint32_t pngPixelLength = 0;
    uint32_t pngPixelWidth = 0;
    uint32_t crc_exp = 0;
    uint32_t temp = 0;
    U64 len_def = 0;
    U64 len_inf = 0;

    uint32_t pnglengths[argc - 1]; // array of png lengths
    memset( pnglengths, 0, (argc-1)*sizeof(uint32_t) ); // initialize array to 0

    
    U64 datalengths[argc - 1]; // array of chunk data lengths
    memset( datalengths, 0, (argc-1)*sizeof(uint32_t) ); // initialize array to 0
    
    U8 *idats[argc - 1]; // array of pointers to png's IDATs 
    for(int i = 0; i < argc - 1; i++) { idats[i] = NULL; } // initialize all pointers to null

    p_hbuffer = malloc(HEADER_BYTES + IHDR_BYTES); // allocate space for File Header and IHDR
    if (p_hbuffer == NULL) {
        perror("malloc");
        return errno;
    }

    p_lbuffer = malloc( LEN_BYTES );
    if (p_lbuffer == NULL) {
        perror("malloc");
        return errno;
    }

    for(int i = 0; i < argc-1; i++){

        char* filename = argv[i+1];
        printf("\nfile %s, entry %d\n", filename, i);

        fp_png = fopen(filename, "r"); // open next png file to verify and concatenate
        if(fp_png == NULL){
            printf("Failed to open file %s \n", filename);
            return -1;
        }
        printf("Successfully opened file %s \n", filename);

        /* Storing entirety of header and IHDR chunk into p_buffer*/
        fread(p_hbuffer, 1, HEADER_BYTES + IHDR_BYTES, fp_png);

        // validate IHDR
        if(!validate_ihdr(p_hbuffer)){
            printf("IHDR validation for file %s failed.\n", filename);
        }
        printf("IHDR validation for file %s succesful\n", filename);

        if(i == 0){
            pngPixelWidth = get_png_width(p_hbuffer);
        }

        pnglengths[i] = get_png_length(p_hbuffer);

        fread(p_lbuffer, 1, LEN_BYTES, fp_png);
        datalengths[i] = get_data_length(p_lbuffer);

        idats[i] = get_idat(fp_png, datalengths[i]);

        fclose(fp_png);
        fp_png = NULL;  
    }


    for(int i = 0; i < argc-1; i++){
        ret = mem_inf(&gp_buf_inf[bytesIngested], &len_inf, idats[i], datalengths[i]);
        if (ret != 0) { /* failure */
            fprintf(stderr,"mem_inf failed. ret = %d.\n", ret);
        }

        bytesIngested += (int) len_inf;
        pngDataLength += datalengths[i];
        pngPixelLength += pnglengths[i];
    }

    ret = mem_def(gp_buf_def, &len_def, gp_buf_inf, bytesIngested, Z_DEFAULT_COMPRESSION);
    if (ret != 0){ /* failure */
        fprintf(stderr,"mem_def failed. ret = %d.\n", ret);
        // return ret;
    }

    p_allbuffer = malloc( 41+len_def+16 );

    // copy in the correct PNG Header
    memcpy(p_allbuffer, correctsignature, 8);
    
    // copy in the correct IHDR Chunk
    memcpy(&p_allbuffer[8], correctIHDR, 21);

    // correct the incorrect width field in the IHDR
    temp = htonl(pngPixelWidth);
    memcpy(&p_allbuffer[16], &temp, 4);

    // correct the incorrect height field in the IHDR
    temp = htonl(pngPixelLength);
    memcpy(&p_allbuffer[20], &temp, 4); 

    // calculate and copy in the correct IHDR CRC
    crc_exp = crc(&p_allbuffer[HEADER_BYTES + LEN_BYTES], TYPE_BYTES + IHDR_DATA_BYTES);
    temp = htonl(crc_exp);
    memcpy(&p_allbuffer[29], &temp, 4);

    // copy in idat chunk length field
    temp = htonl(len_def);
    memcpy(&p_allbuffer[33], &temp, 4);

    // copy in idat chunk type field
    memcpy(&p_allbuffer[37], correctIDAT, 4);

    // copy in compressed IDAT data
    memcpy(&p_allbuffer[41], gp_buf_def, len_def);

    // copy in idat chunk crc field
    crc_exp = crc(&p_allbuffer[HEADER_BYTES + LEN_BYTES], TYPE_BYTES + IHDR_DATA_BYTES);
    temp = htonl(crc_exp);
    memcpy(&p_allbuffer[41+len_def], &temp, 4);

    // copy in correct IEND Chunk
    memcpy(&p_allbuffer[41+len_def+4], correctIEND, 12);

    free(p_hbuffer);
    p_hbuffer = NULL;

    free(p_lbuffer);
    p_lbuffer = NULL;

    /* Write everything into all.png */
    
    fp_all = fopen("all.png", "w"); // open all.png that we will be writing into
    if(fp_all == NULL){
        printf("\nFailed to open/create file all.png\n");
        return -1;
    }
    printf("\nSuccessfully created/opened file all.png\n");

    fwrite( p_allbuffer, 1, 41+len_def+16, fp_all );

    fclose(fp_all);
    fp_all = NULL;

    printf("\n");

    return 0;
}